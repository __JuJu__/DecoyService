﻿using System;
using System.Collections.Generic;
using System.Timers;
using System.Diagnostics;
using System.Configuration;

namespace DecoyService
{
    public class DecoyService : System.ServiceProcess.ServiceBase
    {
        static List<User> ListDecoyUsers = DecoysInteraction.AllDecoyUsers();
        static protected int size = ListDecoyUsers.Count;

        public int eventId = 1;
        private EventLog myLog = new EventLog();
        Timer timer = new Timer();

        string TARGET_SERVER = ConfigurationManager.AppSettings.Get("target_server");
        string DOMAIN_NAME = ConfigurationManager.AppSettings.Get("domain_name");


        public DecoyService()
        {

            this.ServiceName = "DecoyService";
            this.CanStop = true;
            this.CanPauseAndContinue = true;

            if (!EventLog.SourceExists("DecoyService"))
            {
                EventLog.CreateEventSource("DecoyService", "DecoyLog");
                return;
            }
           
            myLog.Source = "DecoyService";
            myLog.Log = "DecoyLog";

        }

        public static void Main()
        {
            System.ServiceProcess.ServiceBase.Run(new DecoyService());
            
        }

        protected override void OnStart(string[] args)
        {
            
            timer.Interval = 2000;
            timer.Enabled = true;
            timer.Elapsed += new ElapsedEventHandler(this.LaunchLogon);
            timer.Start();
            myLog.WriteEntry("Start the Decoy Service.");
        }
        protected override void OnStop()
        {
            myLog.WriteEntry("Stop the animation of decoy objects");
        }
        public void LaunchLogon(object sender, ElapsedEventArgs args)
        {

            DateTime date1 = DateTime.Now;
            Random rndSeconds = new Random();
            int sIndex = rndSeconds.Next(6000, 30_000);
            //Random rndTime = new Random();
            //int tIndex = rndTime.Next(1, 240);

            //DateTime date2 = date1.AddHours(tIndex).AddSeconds(hIndex);
            DateTime date2 = date1.AddSeconds(sIndex);
            if (date2.Hour < 22 && date2.Hour > 4)
            {
                timer.Interval = sIndex;
                Random rnd = new Random();
                int mIndex = rnd.Next(size);
                String user = ListDecoyUsers[mIndex].SamName;
                string pwd = ListDecoyUsers[mIndex].password;
                myLog.WriteEntry($"{user} is being updated", EventLogEntryType.Information, eventId++);
                if (!DecoysInteraction.IncrementLogonToDecoyUser(user, DOMAIN_NAME, pwd, TARGET_SERVER))
                {
                    myLog.WriteEntry($"{user} failed to update", EventLogEntryType.Information, eventId++);
                }
            }

        }

        private void InitializeComponent()
        {
            // 
            // DecoyService
            // 
            this.AutoLog = false;
            this.ServiceName = "DecoyService";

        }
    }
}
