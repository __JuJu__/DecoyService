﻿using CsvHelper;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices.Protocols;
using System.Globalization;
using System.IO;
using System.Net;


namespace DecoyService
{
    class DecoysInteraction
    {

        //internal  const string TARGET_SERVER = "dc01.ciavaldini.fr";
        //internal  string DOMAIN_NAME = "ciavaldini";
        //public  string DECOY_CSV; // = @"C:\Users\julien\Documents\WindowsPowerShell\Modules\DecoyUsers.csv";


        public static List<User> AllDecoyUsers()
        {
            List<User> ListUser = new List<User>();
            var csvConfig = new CsvConfiguration(CultureInfo.CurrentCulture)
            {
                HasHeaderRecord = true
            };

            string CSV = ConfigurationManager.AppSettings.Get("csv_file");
            string CurDir = AppDomain.CurrentDomain.BaseDirectory;
            string DECOY_CSV = CurDir + CSV;

            if (File.Exists(DECOY_CSV))
            {
                using (TextReader reader = File.OpenText(DECOY_CSV))
                {
                    CsvReader csv = new CsvReader(reader, csvConfig);
                    while (csv.Read())
                    {
                        User Record = csv.GetRecord<User>();
                        ListUser.Add(Record);
                    }
                }
                return ListUser;
            }
            else
            {
                return ListUser;
            }
        }

        public static bool IncrementLogonToDecoyUser(string username, string domain, string password, string url)
        {
            var credentials = new NetworkCredential(username, password, domain);
            var serverId = new LdapDirectoryIdentifier(url, 389);
            var connection = new LdapConnection(serverId, credentials);

            try
            {
                connection.Bind();
                connection.Dispose();
                return true;
            }
            catch (Exception e)
            {
                connection.Dispose();
                return false;
            }
        }

    }
}
