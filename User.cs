﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoyService
{
    public class User
    {
        public string LastName { get; set; } //"LastName","FirstName","password"
        public string FirstName { get; set; }
        public string password { get; set; }
        public string SamName { get { return $"{this.LastName}.{this.FirstName}"; } }
    }
}
