﻿# Orchestration of dummy accounts

This application is produced with Visual Studio 2019 and written in C#.

---

This application creates a **service** that runs in the background and creates some interaction with the accounts that are specified in a **CSV file** (all the configuration is done via App.config).

This interaction is **random in time**, a check is performed on the Hour range to be on office hours on the local computer.

The account with which we interact is chosen randomly.

In the mean time, a dedicated **Log File**(DecoyLog) and a **Log Source**(DecoyService) are created. The displayed named in Service.msc is **DecoyUsersLogonIncrement**.

The procedure is simple:
1. Install the service
2. Start the service from service.msc as admin

## Requirements
- Windows 10
- Visual Studio 2019 Community Edition
- C#
- .NET 5

## Configure the interaction with the AD

Beforehand, the dummy accounts have to be created.
The CSV file should be properly tailored as in DecoyUsers.csv.

Three variables have to be changed accordingly to your needs in **App.config**:
- **TARGET_SERVER**: which targets the DC
- **DOMAIN_NAME**: which targets the domain name
- **CSV_FILE**: which targets the CSV file

Then compile the solution with Visual Studio.


## Installation of the service

As an administrator (InstallUtil.exe is either in your Path or in C:\Windows\Microsoft.NET\Framework\v4.0.30319\InstallUtil.exe) :

```powershell
 InstallUtil.exe  .\bin\Debug\DecoyService.exe
```
## Remove the service
As an administrator:
```powershell
 InstallUtil.exe  -u .\bin\Debug\DecoyService.exe
```
## Launch the service
As an administrator:

From service.msc, search the Decoy service and start!

## Links
[Install Services](https://docs.microsoft.com/en-us/dotnet/framework/windows-services/how-to-install-and-uninstall-services)