﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace DecoyService
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
            //addition
            EventLogInstaller DefaultInstaller = null;
            foreach (Installer installer in serviceInstaller1.Installers)
            {
                if (installer is EventLogInstaller)
                {
                    DefaultInstaller = (EventLogInstaller)installer;
                    break;
                }
            }
            if (DefaultInstaller != null)
            {
                serviceInstaller1.Installers.Remove(DefaultInstaller);
            }
            //end addition
        }
    }
}
